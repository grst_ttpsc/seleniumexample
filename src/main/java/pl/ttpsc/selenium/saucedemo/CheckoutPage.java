package pl.ttpsc.selenium.saucedemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckoutPage extends BasePage {
    @FindBy(id = "first-name")
    private WebElement inputFirstName;
    @FindBy(id = "last-name")
    private WebElement inputLastName;
    @FindBy(id = "postal-code")
    private WebElement inputPostalCode;
    @FindBy(id = "continue")
    private WebElement btnContinue;
    @FindBy(id = "finish")
    private WebElement btnFinish;
    @FindBy(className = "complete-header")
    private WebElement lblCompleteHeader;

    public CheckoutPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void fillForm(String firstName, String lastName, String postalCode) {
        inputFirstName.sendKeys(firstName);
        inputLastName.sendKeys(lastName);
        inputPostalCode.sendKeys(postalCode);
    }

    public void clickContinue() {
        btnContinue.click();
    }

    public void clickFinish() {
        btnFinish.click();
    }
    public void checkOrderMessage(String expectedHeaderMessage) {
        assertEquals(expectedHeaderMessage, lblCompleteHeader.getText());
    }
}
