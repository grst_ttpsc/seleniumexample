package pl.ttpsc.selenium.saucedemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class CartPage extends BasePage {
    @FindBy(id = "checkout")
    WebElement btnCheckout;

    public CartPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void clickCheckoutBtn() {
        btnCheckout.click();
    }
}
