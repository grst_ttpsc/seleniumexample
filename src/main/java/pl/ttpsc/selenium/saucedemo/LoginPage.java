package pl.ttpsc.selenium.saucedemo;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LoginPage extends BasePage {
    @FindBy(id = "user-name")
    private WebElement inputUsername;
    @FindBy(id = "password")
    private WebElement inputPassword;
    @FindBy(id = "login-button")
    private WebElement btnSubmitLogin;
    @FindBy(xpath = "//*[@data-test='error']")
    private WebElement lblErrorMessage;
    @FindBy(className = "login_container")
    private WebElement formLogin;

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void login(String username, String password) {
        inputUsername.sendKeys(username);
        inputPassword.sendKeys(password);
        btnSubmitLogin.click();
    }

    public void checkErrorMessage(String expectedErrorMessage) {
        assertEquals(expectedErrorMessage, lblErrorMessage.getText());
    }

    public void checkIfLoginFormIsDisplayed() {
        formLogin.isDisplayed();
    }
}
