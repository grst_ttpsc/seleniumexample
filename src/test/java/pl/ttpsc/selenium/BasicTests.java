package pl.ttpsc.selenium;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BasicTests {
    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @Test
    void firstTest() {
        driver.get("https://ttpsc.com/pl/");
        String title = driver.getTitle();
        assertEquals("Cyfrowa transformacja dla Przemysłu 4.0 | Transition Technologies PSC", title);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }
}