package pl.ttpsc.selenium.saucedemo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AdvTests {
    private WebDriver driver;


    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/");
    }

    @Test
    void loginAsStandardUser() {
        //--------- Test data ----------
        String username = "standard_user";
        String password = "secret_sauce";
        //--------- Test steps ---------
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(username, password);
        InventoryPage inventoryPage = new InventoryPage(driver);
        inventoryPage.checkIfNavbarIsDisplayed();
    }

    @Test
    void loginErrorAsLockedOutUser() {
        //--------- Test data ----------
        String username = "locked_out_user";
        String password = "secret_sauce";
        String expectedErrorMessage = "Epic sadface: Sorry, this user has been locked out.";
        //--------- Test steps ---------
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(username, password);
        loginPage.checkErrorMessage(expectedErrorMessage);
    }

    @Test
    void logoutAfterLogIn() {
        //--------- Test data ----------
        String username = "standard_user";
        String password = "secret_sauce";
        //--------- Test steps ---------
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(username, password);
        InventoryPage inventoryPage = new InventoryPage(driver);
        inventoryPage.logout();
        loginPage.checkIfLoginFormIsDisplayed();
    }

    @Test
    void fullBuyProcedure() {
        //--------- Test data ----------
        String username = "standard_user";
        String password = "secret_sauce";
        String firstName = "Imię";
        String lastName = "Nazwisko";
        String postalCode = "65-000";
        String expectedOrderMessage = "Thank you for your order!";
        //--------- Test steps ---------
        LoginPage loginPage = new LoginPage(driver);
        loginPage.login(username, password);
        InventoryPage inventoryPage = new InventoryPage(driver);
        inventoryPage.addToCartSauceLabsBackpack();
        inventoryPage.goToCart();
        CartPage cartPage = new CartPage(driver);
        cartPage.clickCheckoutBtn();
        CheckoutPage checkoutPage = new CheckoutPage(driver);
        checkoutPage.fillForm(firstName, lastName, postalCode);
        checkoutPage.clickContinue();
        checkoutPage.clickFinish();
        checkoutPage.checkOrderMessage(expectedOrderMessage);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }
}
