package pl.ttpsc.selenium.saucedemo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BasicTests {
    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        driver.get("https://www.saucedemo.com/");
    }

    @Test
    void loginAsStandardUser() {
        //--------- Test data ----------
        String username = "standard_user";
        String password = "secret_sauce";
        //--------- Test steps ---------
        driver.findElement(By.id("user-name")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("login-button")).click();
        assertTrue(driver.findElement(By.className("app_logo")).isDisplayed());
    }


    @Test
    void fullBuyProcedure() {
        //--------- Test data ----------
        String username = "standard_user";
        String password = "secret_sauce";
        String firstName = "Imię";
        String lastName = "Nazwisko";
        String postalCode = "65-000";
        String expectedOrderMessage = "Thank you for your order!";
        //--------- Test steps ---------
        driver.findElement(By.id("user-name")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.id("login-button")).click();
        driver.findElement(By.id("add-to-cart-sauce-labs-backpack")).click();
        driver.findElement(By.id("shopping_cart_container")).click();
        driver.findElement(By.id("checkout")).click();
        driver.findElement(By.id("first-name")).sendKeys(firstName);
        driver.findElement(By.id("last-name")).sendKeys(lastName);
        driver.findElement(By.id("postal-code")).sendKeys(postalCode);
        driver.findElement(By.id("continue")).click();
        driver.findElement(By.id("finish")).click();
        assertEquals(expectedOrderMessage, driver.findElement(By.className("complete-header")).getText());
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }
}
